# README #

### Code Exercise ###

Please see deployed instane: https://dccodeexercise.azurewebsites.net/

* Empty Project
* Installed Microsoft.CodeAnalysis.FxCopAnalyzers
* Fixed namespaces
* Projects Structure
* Calculator with tests
* Generic FizBuzz for two parameters
* Generic FizBuzz for multiple parameters
* Exposed in API
* Swagger
* Postman
* About endpoint
* Comments and more UTs.