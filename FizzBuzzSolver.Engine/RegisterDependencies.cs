﻿using FizzBuzzSolver.Engine.Calculator;
using Microsoft.Extensions.DependencyInjection;

namespace FizzBuzzSolver.Engine
{
    public static class RegisterDependencies
    {
        public static void RegisterFizzBuzzDependencies(this IServiceCollection services)
        {
            services.AddTransient<IFizzBuzzCalculator, FizzBuzzCalculator>();
        }
    }
}