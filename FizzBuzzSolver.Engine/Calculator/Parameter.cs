﻿using System;

namespace FizzBuzzSolver.Engine.Calculator
{
    public class Parameter
    {
        public Parameter(int multiplier, string word)
        {
            if (multiplier == 0)
            {
                throw new ArgumentOutOfRangeException(nameof(multiplier), "The Multiplier cannot be 0.");
            }

            if (string.IsNullOrWhiteSpace(word))
            {
                throw new ArgumentNullException(nameof(word), "The Word cannot be null or empty.");
            }

            Multiplier = multiplier;
            Word = word;
        }

        public int Multiplier { get; }
        public string Word { get; }
    }
}