﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace FizzBuzzSolver.Engine.Calculator
{
    internal class FizzBuzzCalculator : IFizzBuzzCalculator
    {
        public List<string> GetAll(MaxNumber max, ParametersCollection parameters)
        {
            if (max == null)
            {
                throw new ArgumentNullException(nameof(max), "The max parameter cannot be null.");
            }

            var result = new List<string>();
            for (var i = 1; i <= max.Value; i++)
            {
                result.Add(GetOne(i, parameters));
            }

            return result;
        }

        public string GetOne(int n, ParametersCollection parameters)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(nameof(parameters), "The parameters cannot be null.");
            }

            var result = new StringBuilder();
            foreach (var parameter in parameters)
            {
                if (n % parameter.Multiplier == 0)
                {
                    result.Append(parameter.Word);
                }
            }

            var nonDividable = result.Length == 0;
            if (nonDividable)
            {
                result.Append(n.ToString(CultureInfo.InvariantCulture));
            }

            return result.ToString();
        }
    }
}
