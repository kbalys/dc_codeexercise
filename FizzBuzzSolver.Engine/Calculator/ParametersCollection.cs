﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace FizzBuzzSolver.Engine.Calculator
{
    public class ParametersCollection : IEnumerable<Parameter>
    {
        public ParametersCollection()
        {
            Params = new List<Parameter>().AsReadOnly();
        }

        public ParametersCollection(IEnumerable<Parameter> parameters)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(nameof(parameters), "Parameters cannot be null.");
            }

            Params = parameters.ToList().AsReadOnly();
        }

        private ReadOnlyCollection<Parameter> Params { get; }

        public IEnumerator<Parameter> GetEnumerator()
        {
            return Params.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}