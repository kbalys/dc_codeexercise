﻿using System;

namespace FizzBuzzSolver.Engine.Calculator
{
    public class MaxNumber
    {
        public MaxNumber(int maxNumber)
        {
            if (maxNumber <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(maxNumber), "The Max Number parameter cannot be negative or zero.");
            }

            if (maxNumber > 1000)
            {
                throw new ArgumentOutOfRangeException(nameof(maxNumber), "The Max Number parameter is too big.");
            }

            Value = maxNumber;
        }

        public int Value { get; private set; }
    }
}
