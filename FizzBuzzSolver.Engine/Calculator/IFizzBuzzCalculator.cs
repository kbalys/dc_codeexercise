﻿using System.Collections.Generic;

namespace FizzBuzzSolver.Engine.Calculator
{
    /// <summary>
    /// Dynamic FizzBuzz solver calculator. 
    /// </summary>
    public interface IFizzBuzzCalculator
    {
        /// <summary>
        /// Gets the list of FizzBuzz results.
        /// </summary>
        /// <param name="max">Max number of items to get.</param>
        /// <param name="parameters">A collection of parameters (Multiplier : Word).</param>
        /// <returns>A list of strings from 1 to Max Number.</returns>
        List<string> GetAll(MaxNumber max, ParametersCollection parameters);
    }
}