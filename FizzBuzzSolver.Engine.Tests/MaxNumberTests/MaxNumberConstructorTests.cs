﻿using System;
using FizzBuzzSolver.Engine.Calculator;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FizzBuzzSolver.Engine.Tests.MaxNumberTests
{
    [TestClass]
    public class MaxNumberConstructorTests
    {
        [DataTestMethod]
        [DataRow(3)]
        [DataRow(6)]
        public void ShouldCreate_WhenValid(int n)
        {
            // arrange and act
            var target = new MaxNumber(n);

            // assert
            target.Value.Should().Be(n);
        }


        [DataTestMethod]
        [DataRow(-1)]
        [DataRow(0)]
        [DataRow(int.MaxValue)]
        public void ShouldThrow_WhenInvalid(int n)
        {
            // arrange & act
            var action = new Action(() => new MaxNumber(n));

            // assert
            action.Should().Throw<ArgumentOutOfRangeException>();
        }
    }
}