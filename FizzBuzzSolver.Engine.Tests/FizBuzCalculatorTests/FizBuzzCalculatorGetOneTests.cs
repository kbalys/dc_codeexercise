using System;
using System.Globalization;
using FizzBuzzSolver.Engine.Calculator;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FizzBuzzSolver.Engine.Tests.FizBuzCalculatorTests
{
    [TestClass]
    public class FizBuzzCalculatorGetOneTests
    {
        private static readonly Parameter Fizz = new Parameter(3, "Fizz");
        private static readonly Parameter Buzz = new Parameter(5, "Buzz");
        private static readonly ParametersCollection FizzBuzz = new ParametersCollection(new [] {Fizz, Buzz});

        [DataTestMethod]
        [DataRow(3, "Fizz")]
        [DataRow(6, "Fizz")]
        [DataRow(9, "Fizz")]
        [DataRow(12, "Fizz")]
        [DataRow(18, "Fizz")]
        public void ShouldReturnFiz_When3Input(int n, string expected)
        {
            // arrange
            var target = new FizzBuzzCalculator();

            // act
            var result = target.GetOne(n, FizzBuzz);

            // assert
            result.Should().Be(expected);
        }

        [DataTestMethod]
        [DataRow(5, "Buzz")]
        [DataRow(10, "Buzz")]
        [DataRow(20, "Buzz")]
        public void ShouldReturnBuzz_When5Input(int n, string expected)
        {
            // arrange
            var target = new FizzBuzzCalculator();

            // act
            var result = target.GetOne(n, FizzBuzz);

            // assert
            result.Should().Be(expected);
        }


        [DataTestMethod]
        [DataRow(15, "FizzBuzz")]
        public void ShouldReturnFizBuzz_When15Input(int n, string expected)
        {
            // arrange
            var target = new FizzBuzzCalculator();

            // act
            var result = target.GetOne(n, FizzBuzz);

            // assert
            result.Should().Be(expected);
        }


        [DataTestMethod]
        [DataRow(1)]
        [DataRow(2)]
        [DataRow(4)]
        [DataRow(7)]
        [DataRow(8)]
        [DataRow(1)]
        public void ShouldReturnNumber_WhenNo3Or5Input(int n)
        {
            // arrange
            var target = new FizzBuzzCalculator();

            // act
            var result = target.GetOne(n, FizzBuzz);

            // assert
            result.Should().Be(n.ToString(CultureInfo.InvariantCulture));
        }

        [TestMethod]
        public void ShouldThrowException_WhenFizBuzzIsNull()
        {
            // arrange
            var target = new FizzBuzzCalculator();

            // act
            var action = new Action(() => target.GetOne(1, null));

            // assert
            action.Should().Throw<ArgumentNullException>().And.Message.Should().Contain("parameters");
        }
    }
}
