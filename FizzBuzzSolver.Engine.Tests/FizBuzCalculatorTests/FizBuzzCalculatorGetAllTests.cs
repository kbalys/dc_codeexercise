﻿using System;
using FizzBuzzSolver.Engine.Calculator;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FizzBuzzSolver.Engine.Tests.FizBuzCalculatorTests
{
    [TestClass]
    public class FizBuzzCalculatorGetAllTests
    {
        [TestMethod]
        public void ShouldThrowException_WhenMaxIsNull()
        {
            // arrange
            var target = new FizzBuzzCalculator();

            // act
            var action = new Action(() => target.GetAll(null, null));

            // assert
            action.Should().Throw<ArgumentNullException>().And.Message.Should().Contain("max");
        }

        [TestMethod]
        public void ShouldReturnNumberOfItems_WhenValueProvided()
        {
            // arrange
            var target = new FizzBuzzCalculator();

            // act
            var result = target.GetAll(new MaxNumber(15), new ParametersCollection());

            // assert
            result.Should().HaveCount(15);
        }
    }
}