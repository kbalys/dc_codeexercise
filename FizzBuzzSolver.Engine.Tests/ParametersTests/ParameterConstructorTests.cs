﻿using System;
using FizzBuzzSolver.Engine.Calculator;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FizzBuzzSolver.Engine.Tests.ParametersTests
{
    [TestClass]
    public class ParameterConstructorTests
    {
        [TestMethod]
        public void ShouldCreate_WhenValid()
        {
            // arrange & act
            var target = new Parameter(3, "a");

            // assert
            target.Multiplier.Should().Be(3);
            target.Word.Should().Be("a");
        }


        [TestMethod]
        public void ShouldThrow_WhenInvalidMultiplier()
        {
            // arrange & act
            var action = new Action(() => new Parameter(0, "a"));

            // assert
            action.Should().Throw<ArgumentOutOfRangeException>();
        }

        [TestMethod]
        public void ShouldThrow_WhenInvalidWord()
        {
            // arrange & act
            var action = new Action(() => new Parameter(3, null));

            // assert
            action.Should().Throw<ArgumentNullException>();
        }
    }
}