﻿using System;
using System.Linq;
using FizzBuzzSolver.Engine.Calculator;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FizzBuzzSolver.Engine.Tests.ParametersTests
{
    [TestClass]
    public class ParametersCollectionConstructorTests
    {
        [TestMethod]
        public void ShouldCreateEmpty_WhenEmpty()
        {
            // arrange & act
            var target = new ParametersCollection();

            // assert
            target.ToList().Count.Should().Be(0);
        }

        [TestMethod]
        public void ShouldCreate_WhenOneParameter()
        {
            // arrange & act
            var target = new ParametersCollection(new[] {new Parameter(2, "a")});

            // assert
            target.ToList().Count.Should().Be(1);
        }

        [TestMethod]
        public void ShouldThrow_WhenEmptyInput()
        {
            // arrange & act
            var action = new Action(() => new ParametersCollection(null));

            // assert
            action.Should().Throw<ArgumentNullException>();
        }
    }
}