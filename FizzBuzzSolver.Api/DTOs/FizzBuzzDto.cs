﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FizzBuzzSolver.Api.DTOs
{
    public class FizzBuzzDto
    {
        [Range(1, 1000)]
        public int MaxNumber { get; set; }

        [Required]
        public Dictionary<int, string> FizzBuzz { get; set; } 
    }
}