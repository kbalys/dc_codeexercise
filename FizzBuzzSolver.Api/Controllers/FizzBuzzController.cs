﻿using System;
using System.Collections.Generic;
using System.Linq;
using FizzBuzzSolver.Api.DTOs;
using FizzBuzzSolver.Engine.Calculator;
using Microsoft.AspNetCore.Mvc;

namespace FizzBuzzSolver.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FizzBuzzController : ControllerBase
    {
        private readonly IFizzBuzzCalculator _calculator;

        public FizzBuzzController(IFizzBuzzCalculator calculator)
        {
            _calculator = calculator ?? throw new ArgumentNullException(nameof(calculator));
        }

        /// <summary>
        /// Gets the FizBuzz results.
        /// </summary>
        /// <param name="requestBody">A body which contains Max Number to return and map of parameters (Multiplier : Word).</param>
        /// <returns>A list of FizzBuzz.</returns>
        [HttpPost]
        public IEnumerable<string> Post(FizzBuzzDto requestBody)
        {
            return _calculator.GetAll(new MaxNumber(requestBody.MaxNumber), new ParametersCollection(requestBody.FizzBuzz.Select(i => new Parameter(i.Key, i.Value))));
        }
    }
}