﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace FizzBuzzSolver.Api.Controllers
{
    [ApiController]
    [Route("/")]
    public class AboutController : ControllerBase
    {
        /// <summary>
        /// Gets the About Info.
        /// </summary>
        /// <returns>A string info.</returns>
        [HttpGet]
        public string Get()
        {
            return $"{DateTime.UtcNow:u}\nFizzBuzz Solver\nCreated by Konrad Bałys (Future Processing S.A.)";
        }
    }
}